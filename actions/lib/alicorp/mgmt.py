import sys

from st2common.runners.base_action import Action

class MGMTAction(Action):
    def run(self, cmd):
        print(">>> parameter received inner full-alicorp dir", cmd)

        if cmd == 'restart':
            return (True, cmd)
        return (False, cmd)
