import sys

from st2common.runners.base_action import Action

import sys
sys.path.insert(0,'./libs')

import psycopg2
import psycopg2.extras

import base64
import io
import paramiko
from sshtunnel import SSHTunnelForwarder
import os
import random

from prettytable import PrettyTable
import string

bastion_server = "18.189.143.173"
bastion_user = "database"
tunnel = SSHTunnelForwarder
database_server = "mplus2-production-rds-postgresql.cxj3cum2uaas.us-east-2.rds.amazonaws.com"  #database_server = "10.0.22.73"
database_port = 5432
custom_database_port = random.randint(1111, 5000)
database_user = "ureader"
database_name = "jirendb"
database_password = "$u$reader$"
pem_file_name = "production.tunnel-production-key-bastion"
localhost = "127.0.0.1"


from st2common.runners.utils import PackConfigDict


class ReportsAction(Action):
    def get_ssh_key_pem_encoded(self):
        #https://number1.co.za/stackstorm-configurating-a-pack-and-using-configuration-in-actions/
        #pem_file = os.getenv("SSH_PEM_FILE", False)
        pem_file = self.config.get("private_key_path", False)
        if pem_file:
            return pem_file
        with open(f"{os.getcwd()}/{pem_file_name}", "rb") as f:
            return base64.b64encode(f.read())

    def get_ssh_key(self):
        ssh_key_blob_decoded = base64.b64decode(self.get_ssh_key_pem_encoded())
        return ssh_key_blob_decoded.decode("utf-8")

    def get_tunnel(self, proxy_server, proxy_user):
        pem = paramiko.RSAKey.from_private_key(io.StringIO(self.get_ssh_key()))
        return SSHTunnelForwarder(
                proxy_server, ssh_username=proxy_user,
                ssh_pkey=pem,
                remote_bind_address=(database_server, database_port),
                local_bind_address=(localhost, custom_database_port)
                )

    def get_connection_info(self, tunnel):
        return dict(
                host=tunnel.local_bind_host,
                port=tunnel.local_bind_port,
                database=database_name,
                user=database_user,
                password=database_password
                )

    def print_in_table(self, colnames, rows):
        #https://stackoverflow.com/questions/9535954/printing-lists-as-tabular-data
        table = PrettyTable([c.upper() for c in colnames])

        for r in rows:
            data = []
            for c in colnames:
                data.append(r.get(c))
            table.add_row(data)

        #print(table)
        return table

    def get_entity(self, entity, top):
        entity_dict = dict(
            sales = f"""
                select p.pes_fecha_registro, p.pes_precio_compra_confirmado, p.pes_tpa_codigo, p.pes_estado
                from pedidos_sponsors as p
                limit {top or 10};
                """,
            products = f"""

                SELECT p.pal_nombre as "Nombre", p.pal_precio_lista as "Precio", p.pal_cantidad_disponible as "Stock"
                from productos_almacen as p
                limit {top or 10}
                """
                )
        query = entity_dict[entity]
        return query

    def lambda_handler(self, entity, top):
        query = self.get_entity(entity, top)
        tunnel = self.get_tunnel(bastion_server, bastion_user)
        tunnel.start()
        conn = psycopg2.connect(**self.get_connection_info(tunnel))
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute(query)
        colnames = [desc[0] for desc in cur.description]
        rows = cur.fetchall()
        table = self.print_in_table(colnames, rows)

        #print(table)
        tunnel.stop()
        return table

    def run(self, entity, top=10):
        print(f">>> send report {entity} {top or 10} top items")
        table = self.lambda_handler(entity, top)
        print(table)

        if table:
            return (True, entity)
        return (False, entity)

if __name__ == '__main__':
    action = ReportsAction()
    action("sales")
